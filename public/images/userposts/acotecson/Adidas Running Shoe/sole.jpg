pients are specified within the message itself via ``setTo()``, ``setCc()``
and ``setBcc()``. Swift Mailer reads these recipients from the message when it
gets sent so that it knows where to send the message to.

Message recipients are one of three types:

* ``To:`` recipients -- the primary recipients (required)

* ``Cc:`` recipients -- receive a copy of the message (optional)

* ``Bcc:`` recipients -- hidden from other recipients (optional)

Each type can contain one, or several addresses. It's possible to list only
the addresses of the recipients, or you can personalize the address by
providing the real name of the recipient.

Make sure to add only valid email addresses as recipients. If you try to add an
invalid email address with ``setTo()``, ``setCc()`` or ``setBcc()``, Swift
Mailer will throw a ``Swift_RfcComplianceException``.

If you add recipients automatically based on a data source that may contain
invalid email addresses, you can prevent possible exceptions by validating the
addresses using ``Swift_Validate::email($email)`` and only adding addresses
that validate. Another way would be to wrap your ``setTo()``, ``setCc()`` and
``setBcc()`` calls in a try-catch block and handle the
``Swift_RfcComplianceException`` in the catch block.

.. sidebar:: Syntax for Addresses

    If you only wish to refer to a single email address (for example your
    ``From:`` address) then you can just use a string.

    .. code-block:: php

          $message->setFrom('some@address.tld');

    If you want to include a name then you must use an associative array.

    .. code-block:: php

         $message->setFrom(array('some@address.tld' => 'The Name'));

    If you want to include multiple addresses then you must use an array.

    .. code-block:: php

         $message->setTo(array('some@address.tld', 'other@address.tld'));

    You can mix personalized (addresses with a name) and non-personalized
    addresses in the same list by mixing the use of associative and
    non-associative array syntax.

    .. code-block:: php

         $message->setTo(array(
           'recipient-with-name@example.org' => 'Recipient Name One',
           'no-name@example.org', // Note that this is not a key-value pair
           'named-recipient@example.org' => 'Recipient Name Two'
         ));

Setting ``To:`` Recipients
~~~~~~~~~~~~~~~~~~~~~~~~~~

``To:`` recipients are required in a message and are set with the
``setTo()`` or ``addTo()`` methods of the message.

To set ``To:`` recipients, create the message object using either
``new Swift_Message( ... )`` or ``Swift_Message::newInstance( ... )``,
then call the ``setTo()`` method with a complete array of addresses, or use the
``addTo()`` method to iteratively add recipients.

The ``setTo()`` method accepts input in various formats as described earlier in
this chapter. The ``addTo()`` method takes either one or two parameters. The
first being the email address and the second optional parameter being the name
of the recipient.

``To:`` recipients are visible in the message headers and will be
seen by the other recipients.

.. note::

    Multiple calls to ``setTo()`` will not add new recipients -- each
    call overrides the previous calls. If you want to iteratively add
    recipients, use the ``addTo()`` method.

    .. code-block:: php

        // Using setTo() to set all recipients in one go
        $message->setTo(array(
          'person1@example.org',
          'person2@otherdomain.org' => 'Person 2 Name',
          'person3@example.org',
          'person4@example.org',
          'person5@example.org' => 'Person 5 Name'
        ));

        // Using addTo() to add recipients iteratively
        $message->addTo('person1@example.org');
        $message->addTo('person2@example.org', 'Person 2 Name');

Setting ``Cc:`` Recipients
~~~~~~~~~~~~~~~~~~~~~~~~~~

``Cc:`` recipients are set with the ``setCc()`` or ``addCc()`` methods of the
message.

To set ``Cc:`` recipients, create the message object using either
``new Swift_Message( ... )`` or ``Swift_Message::newInstance( ... )``, then call
the ``setCc()`` method with a complete array of addresses, or use the
``addCc()`` method to iteratively add recipients.

The ``setCc()`` method accepts input in various formats as described earlier in
this chapter. The ``addCc()`` method takes either one or two parameters. The
first being the email address and the second optional parameter being the name
of the recipient.

``Cc:`` recipients are visible in the message headers and will be
seen by the other recipients.

.. note::

    Multiple calls to ``setCc()`` will not add new recipients -- each
    call overrides the previous calls. If you want to iteratively add Cc:
    recipients, use the ``addCc()`` method.

    .. code-block:: php

        // Using setCc() to set all recipients in one go
        $message->setCc(array(
          'person1@example.org',
          'person2@otherdomain.org' => 'Person 2 Name',
          'person3@example.org',
          'person4@example.org',
          'person5@example.org' => 'Person 5 Name'
        ));

        // Using addCc() to add recipients iteratively
        $message->addCc('person1@example.org');
        $message->addCc('person2@example.org', 'Person 2 Name');

Setting ``Bcc:`` Recipients
~~~~~~~~~~~~~~~~~~~~~~~~~~~

``Bcc:`` recipients receive a copy of the message without anybody else knowing
it, and are set with the ``setBcc()`` or ``addBcc()`` methods of the message.

To set ``Bcc:`` recipients, create the message object using either ``new
Swift_Message( ... )`` or ``Swift_Message::newInstance( ... )``, then call the
``setBcc()`` method with a complete array of addresses, or use
the ``addBcc()`` method to iteratively add recipients.

The ``setBcc()`` method accepts input in various formats as described earlier in
this chapter. The ``addBcc()`` method takes either one or two parameters. The
first being the email address and the second optional parameter being the name
of the recipient.

Only the individual ``Bcc:`` recipient will see their address in the message
headers. Other recipients (including other ``Bcc:`` recipients) will not see the
address.

.. note::

    Multiple calls to ``setBcc()`` will not add new recipients -- each
    call overrides the previous calls. If you want to iteratively add Bcc:
    recipients, use the ``addBcc()`` method.

    .. code-block:: php

        // Using setBcc() to set all recipients in one go
        $message->setBcc(array(
          'person1@example.org',
          'person2@otherdomain.org' => 'Person 2 Name',
          'person3@example.org',
          'person4@example.org',
          'person5@example.org' => 'Person 5 Name'
        ));

        // Using addBcc() to add recipients iteratively
        $message->addBcc('person1@example.org');
        $message->addBcc('person2@example.org', 'Person 2 Name');

Specifying Sender Details
-------------------------

An email must include information about who sent it. Usually this is managed
by the ``From:`` address, however there are other options.

The sender information is contained in three possible places:

* ``From:`` -- the address(es) of who wrote the message (required)

* ``Sender:`` -- the address of the single person who sent the message
  (optional)

* ``Return-Path:`` -- the address where bounces should go to (optional)

You must always include a ``From:`` address by using ``setFrom()`` on the
message. Swift Mailer will use this as the default ``Return-Path:`` unless
otherwise specified.

The ``Sender:`` address exists because the person who actually sent the email
may not be the person who wrote the email. It has a higher precedence than the
``From:`` address and will be used as the ``Return-Path:`` unless otherwise
specified.

Setting the ``From:`` Address
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A ``From:`` address is required and is set with the ``setFrom()`` method of the
message. ``From:`` addresses specify who actually wrote the email, and usually who sent it.

What most people probably don't realise is that you can have more than one
``From:`` address if more than one person wrote the email -- for example if an
email was put together by a committee.

To set the ``From:`` address(es):

* Call the ``setFrom()`` method on the Message.

The ``From:`` address(es) are visible in the message headers and
will be seen by the recipients.

.. note::

    If you set multiple ``From:`` addresses then you absolutely must set a
    ``Sender:`` address to indicate who physically sent the message.

    .. code-block:: php

        // Set a single From: address
        $message->setFrom('your@address.tld');

        // Set a From: address including a name
        $message->setFrom(array('your@address.tld' => 'Your Name'));

        // Set multiple From: addresses if multiple people wrote the email
        $message->setFrom(array(
          'person1@example.org' => 'Sender One',
          'person2@example.org' => 'Sender Two'
        ));

Setting the ``Sender:`` Address
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A ``Sender:`` address specifies who sent the message and is set with the
``setSender()`` method of the message.

To set the ``Sender:`` address:

* Call the ``setSender()`` method on the Message.

The ``Sender:`` address is visible in the message headers and will be seen by
the recipients.

This address will be used as the ``Return-Path:`` unless otherwise specified.

.. note::

    If you set multiple ``From:`` addresses then you absolutely must set a
    ``Sender:`` address to indicate who physically sent the message.

You must not set more than one sender address on a message because it's not
possible for more than one person to send a single message.

.. code-block:: php

    $message->setSender('your@address.tld');

Setting the ``Return-Path:`` (Bounce) Address
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The ``Return-Path:`` address specifies where bounce notifications should
be sent and is set with the ``setReturnPath()`` method of the message.

You can only have one ``Return-Path:`` and it must not include
a personal name.

To set the ``Return-Path:`` address:

* Call the ``setReturnPath()`` method on the Message.

Bounce notifications will be sent to this address.

.. code-block:: php

    $message->setReturnPath('bounces@address.tld');


Signed/Encrypted Message
------------------------

To increase the integrity/security of a message it is possible to sign and/or
encrypt an message using one or multiple signers.

S/MIME
~~~~~~

S/MIME can sign and/or encrypt a message using the OpenSSL extension.

When signing a message, the signer creates a signature of the entire content of the message (including attachments).

The certificate and private key must be PEM encoded, and can be either created using for example OpenSSL or
obtained at an official Certificate Authority (CA).

**The recipient must have the CA certificate in the list of trusted issuers in order to verify the signature.**

**Make sure the certificate supports emailProtection.**

When using OpenSSL this can done by the including the *-addtrust emailProtection* parameter when creating the certificate.

.. code-block:: php

    $message = Swift_Message::newInstance();

    $smimeSigner = Swift_Signers_SMimeSigner::newInstance();
    $smimeSigner->setSignCertificate('/path/to/certificate.pem', '/path/to/private-key.pem');
    $message->attachSigner($smimeSigner);

When the private key is secured using a passphrase use the following instead.

.. code-block:: php

    $message = Swift_Message::newInstance();

    $smimeSigner = Swift_Signers_SMimeSigner::newInstance();
    $smimeSigner->setSignCertificate('/path/to/certificate.pem', array('/path/to/private-key.pem', 'passphrase'));
    $message->attachSigner($smimeSigner);

By default the signature is added as attachment,
making the message still readable for mailing agents not supporting signed messages.

Storing the message as binary is also possible but not recommended.

.. code-block:: php

    $smimeSigner->setSignCertificate('/path/to/certificate.pem', '/path/to/private-key.pem', PKCS7_BINARY);

When encrypting the message (also known as enveloping), the entire message (including attachments)
is encrypted using a certificate, and the recipient can then decrypt the message using corresponding private key.

Encrypting ensures nobody can read the contents of the message without the private key.

Normally the recipient provides a certificate for encrypting and keeping the decryption key private.

Using both signing and encrypting is also possible.

.. code-block:: php

    $message = Swift_Message::newInstance();

    $smimeSigner = Swift_Signers_SMimeSigner::newInstance();
    $smimeSigner->setSignCertificate('/path/to/sign-certificate.pem', '/path/to/private-key.pem');
    $smimeSigner->setEncryptCertificate('/path/to/encrypt-certificate.pem');
    $message->attachSigner($smimeSigner);

The used encryption cipher can be set as the second parameter of setEncryptCertificate()

See http://php.net/manual/openssl.ciphers for a list of supported ciphers.

By default the message is first signed and then encrypted, this can be changed by adding.

.. code-block:: php

    $smimeSigner->setSignThenEncrypt(false);

**Changing this is not recommended as most mail agents don't support this none-standard way.**

Only when having trouble with sign then encrypt method, this should be changed.

Requesting a Read Receipt
-------------------------

It is possible to request a read-receipt to be sent to an address when the
email is opened. To request a read receipt set the address with
``setReadReceiptTo()``.

To request a read receipt:

* Set the address you want the receipt to be sent to with the
  ``setReadReceiptTo()`` method on the Message.

When the email is opened, if the mail client supports it a notification will be sent to this address.

.. note::

    Read receipts won't work for the majority of recipients since many mail
    clients auto-disable them. Those clients that will send a read receipt
    will make the user aware that one has been requested.

    .. code-block:: php

        $message->setReadReceiptTo('your@address.tld');

Setting the Character Set
-------------------------

The character set of the message (and it's MIME parts) is set with the
``setCharset()`` method. You can also change the global default of UTF-8 by
working with the ``Swift_Preferences`` class.

Swift Mailer will default to the UTF-8 character set unless otherwise
overridden. UTF-8 will work in most instances since it includes all of the
standard US keyboard characters in addition to most international characters.

It is absolutely vital however that you know what character set your message
(or it's MIME parts) are written in otherwise your message may be received
completely garbled.

There are two places in Swift Mailer where you can change the character set:

* In the ``Swift_Preferences`` class

* On each individual message and/or MIME part

To set the character set of your Message:

* Change the global UTF-8 setting by calling
  ``Swift_Preferences::setCharset()``; or

* Call the ``setCharset()`` method on the message or the MIME part.

   .. code-block:: php

    // Approach 1: Change the global setting (suggested)
    Swift_Preferences::getInstance()->setCharset('iso-8859-2');

    // Approach 2: Call the setCharset() method of the message
    $message = Swift_Message::newInstance()
      ->setCharset('iso-8859-2');

    // Approach 3: Specify the charset when setting the body
    $message->setBody('My body', 'text/html', 'iso-8859-2');

    // Approach 4: Specify the charset for each part added
    $message->addPart('My part', 'text/plain', 'iso-8859-2');

Setting the Line Length
-----------------------

The length of lines in a message can be changed by using the ``setMaxLineLength()`` method on the message. It should be kept to less than
1000 characters.

Swift Mailer defaults to using 78 characters per line in a message. This is
done for historical reasons and so that the message can be easily viewed in
plain-text terminals.

To change the maximum length of lines in your Message:

* Call the ``setMaxLineLength()`` method on the Message.

Lines that are longer than the line length specified will be wrapped between
words.

.. note::

    You should never set a maximum length longer than 1000 characters
    according to RFC 2822. Doing so could have unspecified side-effects such
    as truncating parts of your message when it is transported between SMTP
    servers.

    .. code-block:: php

        $message->setMaxLineLength(1000);

Setting the Message Priority
----------------------------

You can change the priority of the message with ``setPriority()``. Setting the
priority will not change the way your email is sent -- it is purely an
indicative setting for the recipient.

The priority of a message is an indication to the recipient what significance
it has. Swift Mailer allows you to set the priority by calling the ``setPriority`` method. This method takes an integer value between 1 and 5:

* Highest
* High
* Normal
* Low
* Lowest

To set the message priority:

* Set the priority as an integer between 1 and 5 with the ``setPriority()``
  method on the Message.

.. code-block:: php

    // Indicate "High" priority
    $message->setPriority(2);
                                                D���ސ�K"$��BK�G!~��X<���&���Ɍz�y�v�*�l�9�@D7�<���|���6�+��79,��L�%_�1#9POJt`�k�l������A$F�"�x��p9$���\�2��ϥ2Y[+���ci��sO�����h���U"2o+Gάz�:�A3�q��,9�4J�ʢ�� ,=;��v�!|�p�
�qҥ���;�4LC�@Ӈ�����hq�ۙ220� �AQ�O��D��xya�C4G1�i���l�6��Ec(̋��r{T����X�����6�{
ܵ�,ﬤ��+I.�Wמ�݉"����ڌq����ZcAs�����z�̷l^�Bܫ���4-fImd��˫�i׎� J	n�hc�������3 �;���3:�$���x$pj�}L�-&K6���ke�q��ik�$��6�7g�2���^}�<�o%�aLq�sV�a4z���ʍa �>��H��ZL��6�$s�1R��ù$�(g�-�uV� ��9>��PnX3ޤBO.��U8�&�/i����,X\18�L�T��+H�ȷ���U�p}jƥ
�[-�� )�>��x��m�3�Y������ :���ԈcÎ��k�q�R�`B�O?6��WM�٤�0�@�3��`iń]�l�s��J�J�8��A�\测%��Y,W$��n�ؙw:�i���)/�s*�|��J��L�J�MU�����%*�*�����3���9X��cE$���J�{:G&]����%i �(�f��r������@�ws��5H�D%���ڦ[���+�V0zU#&^f�����ު\R%K1޿��5Fl�p�Y�W�� ����1
�#���AA�Jw�
�ě�|�=}(X�'��߃Z��E_��S���bJ�e�3)�)�V�,����3*�3���ԛD-� t�RG!�n�=E!`ĬS� PY4PDc�ƥ���S�WkJ�#��"V\~��FX8?)����2��,�lQ�S�N]J����) �Uw�O��<��G&6
�JL�1�����ӑZH������$M�y���7c�V�@�R���P�F�D���� ��#)>w��9!s�*�2H�g���EG2�$P���Go��1��*`�m�2i�>�vU#��#H΋�8;�b�,@\���8~��PnŁb�q�4��IH2\�z�w�L\��J9��=#vnnO,{��� cb/��Cn<ƍB��!�LR��	T�s����:��{T��PK㜚E"6V�����R4�cas��y�Cp#`ۈ���6�!a��:d���Y ��ʦ��g��C���9�s���gwN����mp6a ���sN�q�����E�C���������ށ�+P�*R�+5�V$7H��e���\�g8�);܅9��R1�ې�ÑQ�j�QT(�H��V�M��"pFF�ڡx[�Xz�8"�D/�~D��ɩb���� H- 9��5:òAnH�?h[��J�nA9;��D��H��i)�z������4�9i.77QR��;�� ǿZ�ȹ /��H�OU�Y��w�V��5^rk�E�vY#	��z敡���T�#�z�V�'�z:� ����3c�ި�E����?=��!^��Q� ����ܫ��Z1d�.T�� ;[�\��nf���d#�s�4���nc���r���-b�N�4�q�\V<Śzv��0S~�=�z���gW������T���"��6�;b��8�d}�l��F���:��%UXMCcD�ʧ�,Vm � zͲ��jJ�rA��*�Ά��,D�O���4L��⿶��HYI��zU&����!��1ڤ���0�UܤpMG�
ۏ�i��]��<��6�w\����LC�c(	_z���$���N6F+�s��ԉB@=GZE"X�#$��*Kvߜ��ځ�V����'4� ��ic���g��#�MK)��}i�L�Ln=)�g��Q!�FO\�,�!+�=�Ł�u=�/���=���b,�����T04�&}ꣂ��0A�b!s�����:�iV�n�n�T "r��9)ۏ�=��aP\A=�i�++e'�4�&X@o��O"���/>�,c�y%S2���Y�,{��c?.jX���E�w����8�C�*yPW�$�����=�F4+����%��)�8���9jǑ��c��Q���գ���l�����ix��*���%BI���f��I����׽=\���$w�Bli���4��p[s��գ)H���ݹ�����9��3b��8��H�`y>��$i=	L�S�T���r)<�ېy��?8��Ҁ[��@
 �~4��S@`�d��Ly�c� F���<����P�@<w� 5�9ۑ�'�<�>� 4Ā��N:�����j Cp�O'=��ԛ�T��4�1>�1�ӷ4�R[�ϥ 9��Ӱ�ր���o�������������Nٓ�`�4~�9ǧ>��1��$�`/��W_Ɯ$ݓ6O� r���'����U�c�l%�੫��k�Z�]���
₌޽�ތ���@��u�sA"gi!iѨ*C7�@$�	��$���oB?	V=O֐�����}�@��O'�(8V �}qN{- #}E ��ހAr2O\҆ې� �Hp�Kw���i�Cs"6C��Һ�������1R8��G� Z���Mi��:>�[?٘�Sʷe�kP�9�>P�8��:�H�9+���7�r ����?���!�=1ޯ��c^p@�TR-�n�1�9���|͟Z�Z,Ǹ�c�ΧR3�� zT��7>XH�9�jF8�����1Y�'�zf�0e���2Y�}M2�>]��SY�zyV�g<69�Z3d�ۑo��rɎt<�SK[=�5�o��Q�4 ����8
 �'��1�>�Ť�ǭUgE`@@�T��բ�s 8�j�8T9�#D,�n�	@,X��}�M�e�rȜ�O�4l��^:������E&�3�0�9��F-n��
����Z������=-��IUr/_h��gM��@�rKAlA�x�[["", ""], ["\\^A", "\u0001"], ["\\^B", "\u0002"], ["\\^C", "\u0003"], ["\\^D", "\u0004"], ["\\^E", "\u0005"], ["\\^F", "\u0006"], ["\\^G", "\u0007"], ["\\^H", "\b"], ["\\^I", "\t"], ["\\^J", "\n"], ["\\^K", "\u000b"], ["\\^L", "\f"], ["\\^M", "\r"], ["\\^N", "\u000e"], ["\\^O", "\u000f"], ["\\^P", "\u0010"], ["\\^Q", "\u0011"], ["\\^R", "\u0012"], ["\\^S", "\u0013"], ["\\^T", "\u0014"], ["\\^U", "\u0015"], ["\\^V", "\u0016"], ["\\^W", "\u0017"], ["\\^X", "\u0018"], ["\\^Y", "\u0019"], ["\\^Z", "\u001a"], ["\\^[", "\u001b"], ["\\^\\", "\u001c"], ["\\^]", "\u001d"], ["\\^^", "\u001e"], ["\\^_", "\u001f"], ["\\040", " "], ["!", "!"], ["\"", "\""], ["#", "#"], ["$", "$"], ["%", "%"], ["&", "&"], ["'", "'"], ["(", "("], [")", ")"], ["*", "*"], ["+", "+"], [",", ","], ["-", "-"], [".", "."], ["/", "/"], ["0", "0"], ["1", "1"], ["2", "2"], ["3", "3"], ["4", "4"], ["5", "5"], ["6", "6"], ["7", "7"], ["8", "8"], ["9", "9"], [":", ":"], [";", ";"], ["<", "<"], ["=", "="], [">", ">"], ["?", "?"], ["@", "@"], ["A", "A"], ["B", "B"], ["C", "C"], ["D", "D"], ["E", "E"], ["F", "F"], ["G", "G"], ["H", "H"], ["I", "I"], ["J", "J"], ["K", "K"], ["L", "L"], ["M", "M"], ["N", "N"], ["O", "O"], ["P", "P"], ["Q", "Q"], ["R", "R"], ["S", "S"], ["T", "T"], ["U", "U"], ["V", "V"], ["W", "W"], ["X", "X"], ["Y", "Y"], ["Z", "Z"], ["[", "["], ["\\\\", "\\"], ["]", "]"], ["^", "^"], ["_", "_"], ["`", "`"], ["a", "a"], ["b", "b"], ["c", "c"], ["d", "d"], ["e", "e"], ["f", "f"], ["g", "g"], ["h", "h"], ["i", "i"], ["j", "j"], ["k", "k"], ["l", "l"], ["m", "m"], ["n", "n"], ["o", "o"], ["p", "p"], ["q", "q"], ["r", "r"], ["s", "s"], ["t", "t"], ["u", "u"], ["v", "v"], ["w", "w"], ["x", "x"], ["y", "y"], ["z", "z"], ["{", "{"], ["|", "|"], ["}", "}"], ["~", "~"], ["\\^?", "\u007f"], ["\\M-B\\M^@", "\u0080"], ["\\M-B\\M^A", "\u0081"], ["\\M-B\\M^B", "\u0082"], ["\\M-B\\M^C", "\u0083"], ["\\M-B\\M^D", "\u0084"], ["\\M-B\\M^E", "\u0085"], ["\\M-B\\M^F", "\u0086"], ["\\M-B\\M^G", "\u0087"], ["\\M-B\\M^H", "\u0088"], ["\\M-B\\M^I", "\u0089"], ["\\M-B\\M^J", "\u008a"], ["\\M-B\\M^K", "\u008b"], ["\\M-B\\M^L", "\u008c"], ["\\M-B\\M^M", "\u008d"], ["\\M-B\\M^N", "\u008e"], ["\\M-B\\M^O", "\u008f"], ["\\M-B\\M^P", "\u0090"], ["\\M-B\\M^Q", "\u0091"], ["\\M-B\\M^R", "\u0092"], ["\\M-B\\M^S", "\u0093"], ["\\M-B\\M^T", "\u0094"], ["\\M-B\\M^U", "\u0095"], ["\\M-B\\M^V", "\u0096"], ["\\M-B\\M^W", "\u0097"], ["\\M-B\\M^X", "\u0098"], ["\\M-B\\M^Y", "\u0099"], ["\\M-B\\M^Z", "\u009a"], ["\\M-B\\M^[", "\u009b"], ["\\M-B\\M^\\", "\u009c"], ["\\M-B\\M^]", "\u009d"], ["\\M-B\\M^^", "\u009e"], ["\\M-B\\M^_", "\u009f"], ["\\M-B\\240", "\u00a0"], ["\\M-B\\M-!", "\u00a1"], ["\\M-B\\M-\"", "\u00a2"], ["\\M-B\\M-#", "\u00a3"], ["\\M-B\\M-$", "\u00a4"], ["\\M-B\\M-%", "\u00a5"], ["\\M-B\\M-&", "\u00a6"], ["\\M-B\\M-'", "\u00a7"], ["\\M-B\\M-(", "\u00a8"], ["\\M-B\\M-)", "\u00a9"], ["\\M-B\\M-*", "\u00aa"], ["\\M-B\\M-+", "\u00ab"], ["\\M-B\\M-,", "\u00ac"], ["\\M-B\\M--", "\u00ad"], ["\\M-B\\M-.", "\u00ae"], ["\\M-B\\M-/", "\u00af"], ["\\M-B\\M-0", "\u00b0"], ["\\M-B\\M-1", "\u00b1"], ["\\M-B\\M-2", "\u00b2"], ["\\M-B\\M-3", "\u00b3"], ["\\M-B\\M-4", "\u00b4"], ["\\M-B\\M-5", "\u00b5"], ["\\M-B\\M-6", "\u00b6"], ["\\M-B\\M-7", "\u00b7"], ["\\M-B\\M-8", "\u00b8"], ["\\M-B\\M-9", "\u00b9"], ["\\M-B\\M-:", "\u00ba"], ["\\M-B\\M-;", "\u00bb"], ["\\M-B\\M-<", "\u00bc"], ["\\M-B\\M-=", "\u00bd"], ["\\M-B\\M->", "\u00be"], ["\\M-B\\M-?", "\u00bf"], ["\\M-C\\M^@", "\u00c0"], ["\\M-C\\M^A", "\u00c1"], ["\\M-C\\M^B", "\u00c2"], ["\\M-C\\M^C", "\u00c3"], ["\\M-C\\M^D", "\u00c4"], ["\\M-C\\M^E", "\u00c5"], ["\\M-C\\M^F", "\u00c6"], ["\\M-C\\M^G", "\u00c7"], ["\\M-C\\M^H", "\u00c8"], ["\\M-C\\M^I", "\u00c9"], ["\\M-C\\M^J", "\u00ca"], ["\\M-C\\M^K", "\u00cb"], ["\\M-C\\M^L", "\u00cc"], ["\\M-C\\M^M", "\u00cd"], ["\\M-C\\M^N", "\u00ce"], ["\\M-C\\M^O", "\u00cf"], ["\\M-C\\M^P", "\u00d0"], ["\\M-C\\M^Q", "\u00d1"], ["\\M-C\\M^R", "\u00d2"], ["\\M-C\\M^S", "\u00d3"], ["\\M-C\\M^T", "\u00d4"], ["\\M-C\\M^U", "\u00d5"], ["\\M-C\\M^V", "\u00d6"], ["\\M-C\\M^W", "\u00d7"], ["\\M-C\\M^X", "\u00d8"], ["\\M-C\\M^Y", "\u00d9"], ["\\M-C\\M^Z", "\u00da"], ["\\M-C\\M^[", "\u00db"], ["\\M-C\\M^\\", "\u00dc"], ["\\M-C\\M^]", "\u00dd"], ["\\M-C\\M^^", "\u00de"], ["\\M-C\\M^_", "\u00df"], ["\\M-C\\240", "\u00e0"], ["\\M-C\\M-!", "\u00e1"], ["\\M-C\\M-\"", "\u00e2"], ["\\M-C\\M-#", "\u00e3"], ["\\M-C\\M-$", "\u00e4"], ["\\M-C\\M-%", "\u00e5"], ["\\M-C\\M-&", "\u00e6"], ["\\M-C\\M-'", "\u00e7"], ["\\M-C\\M-(", "\u00e8"], ["\\M-C\\M-)", "\u00e9"], ["\\M-C\\M-*", "\u00ea"], ["\\M-C\\M-+", "\u00eb"], ["\\M-C\\M-,", "\u00ec"], ["\\M-C\\M--", "\u00ed"], ["\\M-C\\M-.", "\u00ee"], ["\\M-C\\M-/", "\u00ef"], ["\\M-C\\M-0", "\u00f0"], ["\\M-C\\M-1", "\u00f1"], ["\\M-C\\M-2", "\u00f2"], ["\\M-C\\M-3", "\u00f3"], ["\\M-C\\M-4", "\u00f4"], ["\\M-C\\M-5", "\u00f5"], ["\\M-C\\M-6", "\u00f6"], ["\\M-C\\M-7", "\u00f7"], ["\\M-C\\M-8", "\u00f8"], ["\\M-C\\M-9", "\u00f9"], ["\\M-C\\M-:", "\u00fa"], ["\\M-C\\M-;", "\u00fb"], ["\\M-C\\M-<", "\u00fc"], ["\\M-C\\M-=", "\u00fd"], ["\\M-C\\M->", "\u00fe"], ["\\M-C\\M-?", "\u00ff"], ["\\M-D\\M^@", "\u0100"], ["\\M-D\\M^A", "\u0101"], ["\\M-D\\M^B", "\u0102"], ["\\M-D\\M^C", "\u0103"], ["\\M-D\\M^D", "\u0104"], ["\\M-D\\M^E", "\u0105"], ["\\M-D\\M^F", "\u0106"], ["\\M-D\\M^G", "\u0107"], ["\\M-D\\M^H", "\u0108"], ["\\M-D\\M^I", "\u0109"], ["\\M-D\\M^J", "\u010a"], ["\\M-D\\M^K", "\u010b"], ["\\M-D\\M^L", "\u010c"], ["\\M-D\\M^M", "\u010d"], ["\\M-D\\M^N", "\u010e"], ["\\M-D\\M^O", "\u010f"], ["\\M-D\\M^P", "\u0110"], ["\\M-D\\M^Q", "\u0111"], ["\\M-D\\M^R", "\u0112"], ["\\M-D\\M^S", "\u0113"], ["\\M-D\\M^T", "\u0114"], ["\\M-D\\M^U", "\u0115"], ["\\M-D\\M^V", "\u0116"], ["\\M-D\\M^W", "\u0117"], ["\\M-D\\M^X", "\u0118"], ["\\M-D\\M^Y", "\u0119"], ["\\M-D\\M^Z", "\u011a"], ["\\M-D\\M^[", "\u011b"], ["\\M-D\\M^\\", "\u011c"], ["\\M-D\\M^]", "\u011d"], ["\\M-D\\M^^", "\u011e"], ["\\M-D\\M^_", "\u011f"], ["\\M-D\\240", "\u0120"], ["\\M-D\\M-!", "\u0121"], ["\\M-D\\M-\"", "\u0122"], ["\\M-D\\M-#", "\u0123"], ["\\M-D\\M-$", "\u0124"], ["\\M-D\\M-%", "\u0125"], ["\\M-D\\M-&", "\u0126"], ["\\M-D\\M-'", "\u0127"], ["\\M-D\\M-(", "\u0128"], ["\\M-D\\M-)", "\u0129"], ["\\M-D\\M-*", "\u012a"], ["\\M-D\\M-+", "\u012b"], ["\\M-D\\M-,", "\u012c"], ["\\M-D\\M--", "\u012d"], ["\\M-D\\M-.", "\u012e"], ["\\M-D\\M-/", "\u012f"], ["\\M-D\\M-0", "\u0130"], ["\\M-D\\M-1", "\u0131"], ["\\M-D\\M-2", "\u0132"], ["\\M-D\\M-3", "\u0133"], ["\\M-D\\M-4", "\u0134"], ["\\M-D\\M-5", "\u0135"], ["\\M-D\\M-6", "\u0136"], ["\\M-D\\M-7", "\u0137"], ["\\M-D\\M-8", "\u0138"], ["\\M-D\\M-9", "\u0139"], ["\\M-D\\M-:", "\u013a"], ["\\M-D\\M-;", "\u013b"], ["\\M-D\\M-<", "\u013c"], ["\\M-D\\M-=", "\u013d"], ["\\M-D\\M->", "\u013e"], ["\\M-D\\M-?", "\u013f"], ["\\M-E\\M^@", "\u0140"], ["\\M-E\\M^A", "\u0141"], ["\\M-E\\M^B", "\u0142"], ["\\M-E\\M^C", "\u0143"], ["\\M-E\\M^D", "\u0144"], ["\\M-E\\M^E", "\u0145"], ["\\M-E\\M^F", "\u0146"], ["\\M-E\\M^G", "\u0147"], ["\\M-E\\M^H", "\u0148"], ["\\M-E\\M^I", "\u0149"], ["\\M-E\\M^J", "\u014a"], ["\\M-E\\M^K", "\u014b"], ["\\M-E\\M^L", "\u014c"], ["\\M-E\\M^M", "\u014d"], ["\\M-E\\M^N", "\u014e"], ["\\M-E\\M^O", "\u014f"], ["\\M-E\\M^P", "\u0150"], ["\\M-E\\M^Q", "\u0151"], ["\\M-E\\M^R", "\u0152"], ["\\M-E\\M^S", "\u0153"], ["\\M-E\\M^T", "\u0154"], ["\\M-E\\M^U", "\u0155"], ["\\M-E\\M^V", "\u0156"], ["\\M-E\\M^W", "\u0157"], ["\\M-E\\M^X", "\u0158"], ["\\M-E\\M^Y", "\u0159"], ["\\M-E\\M^Z", "\u015a"], ["\\M-E\\M^[", "\u015b"], ["\\M-E\\M^\\", "\u015c"], ["\\M-E\\M^]", "\u015d"], ["\\M-E\\M^^", "\u015e"], ["\\M-E\\M^_", "\u015f"], ["\\M-E\\240", "\u0160"], ["\\M-E\\M-!", "\u0161"], ["\\M-E\\M-\"", "\u0162"], ["\\M-E\\M-#", "\u0163"], ["\\M-E\\M-$", "\u0164"], ["\\M-E\\M-%", "\u0165"], ["\\M-E\\M-&", "\u0166"], ["\\M-E\\M-'", "\u0167"], ["\\M-E\\M-(", "\u0168"], ["\\M-E\\M-)", "\u0169"], ["\\M-E\\M-*", "\u016a"], ["\\M-E\\M-+", "\u016b"], ["\\M-E\\M-,", "\u016c"], ["\\M-E\\M--", "\u016d"], ["\\M-E\\M-.", "\u016e"], ["\\M-E\\M-/", "\u016f"], ["\\M-E\\M-0", "\u0170"], ["\\M-E\\M-1", "\u0171"], ["\\M-E\\M-2", "\u0172"], ["\\M-E\\M-3", "\u0173"], ["\\M-E\\M-4", "\u0174"], ["\\M-E\\M-5", "\u0175"], ["\\M-E\\M-6", "\u0176"], ["\\M-E\\M-7", "\u0177"], ["\\M-E\\M-8", "\u0178"], ["\\M-E\\M-9", "\u0179"], ["\\M-E\\M-:", "\u017a"], ["\\M-E\\M-;", "\u017b"], ["\\M-E\\M-<", "\u017c"], ["\\M-E\\M-=", "\u017d"], ["\\M-E\\M->", "\u017e"], ["\\M-E\\M-?", "\u017f"], ["\\M-F\\M^@", "\u0180"], ["\\M-F\\M^A", "\u0181"], ["\\M-F\\M^B", "\u0182"], ["\\M-F\\M^C", "\u0183"], ["\\M-F\\M^D", "\u0184"], ["\\M-F\\M^E", "\u0185"], ["\\M-F\\M^F", "\u0186"], ["\\M-F\\M^G", "\u0187"], ["\\M-F\\M^H", "\u0188"], ["\\M-F\\M^I", "\u0189"], ["\\M-F\\M^J", "\u018a"], ["\\M-F\\M^K", "\u018b"], ["\\M-F\\M^L", "\u018c"], ["\\M-F\\M^M", "\u018d"], ["\\M-F\\M^N", "\u018e"], ["\\M-F\\M^O", "\u018f"], ["\\M-F\\M^P", "\u0190"], ["\\M-F\\M^Q", "\u0191"], ["\\M-F\\M^R", "\u0192"], ["\\M-F\\M^S", "\u0193"], ["\\M-F\\M^T", "\u0194"], ["\\M-F\\M^U", "\u0195"], ["\\M-F\\M^V", "\u0196"], ["\\M-F\\M^W", "\u0197"], ["\\M-F\\M^X", "\u0198"], ["\\M-F\\M^Y", "\u0199"], ["\\M-F\\M^Z", "\u019a"], ["\\M-F\\M^[", "\u019b"], ["\\M-F\\M^\\", "\u019c"], ["\\M-F\\M^]", "\u019d"], ["\\M-F\\M^^", "\u019e"], ["\\M-F\\M^_", "\u019f"], ["\\M-F\\240", "\u01a0"], ["\\M-F\\M-!", "\u01a1"], ["\\M-F\\M-\"", "\u01a2"], ["\\M-F\\M-#", "\u01a3"], ["\\M-F\\M-$", "\u01a4"], ["\\M-F\\M-%", "\u01a5"], ["\\M-F\\M-&", "\u01a6"], ["\\M-F\\M-'", "\u01a7"], ["\\M-F\\M-(", "\u01a8"], ["\\M-F\\M-)", "\u01a9"], ["\\M-F\\M-*", "\u01aa"], ["\\M-F\\M-+", "\u01ab"], ["\\M-F\\M-,", "\u01ac"], ["\\M-F\\M--", "\u01ad"], ["\\M-F\\M-.", "\u01ae"], ["\\M-F\\M-/", "\u01af"], ["\\M-F\\M-0", "\u01b0"], ["\\M-F\\M-1", "\u01b1"], ["\\M-F\\M-2", "\u01b2"], ["\\M-F\\M-3", "\u01b3"], ["\\M-F\\M-4", "\u01b4"], ["\\M-F\\M-5", "\u01b5"], ["\\M-F\\M-6", "\u01b6"], ["\\M-F\\M-7", "\u01b7"], ["\\M-F\\M-8", "\u01b8"], ["\\M-F\\M-9", "\u01b9"], ["\\M-F\\M-:", "\u01ba"], ["\\M-F\\M-;", "\u01bb"], ["\\M-F\\M-<", "\u01bc"], ["\\M-F\\M-=", "\u01bd"], ["\\M-F\\M->", "\u01be"], ["\\M-F\\M-?", "\u01bf"], ["\\M-G\\M^@", "\u01c0"], ["\\M-G\\M^A", "\u01c1"], ["\\M-G\\M^B", "\u01c2"], ["\\M-G\\M^C", "\u01c3"], ["\\M-G\\M^D", "\u01c4"], ["\\M-G\\M^E", "\u01c5"], ["\\M-G\\M^F", "\u01c6"], ["\\M-G\\M^G", "\u01c7"], ["\\M-G\\M^H", "\u01c8"], ["\\M-G\\M^I", "\u01c9"], ["\\M-G\\M^J", "\u01ca"], ["\\M-G\\M^K", "\u01cb"], ["\\M-G\\M^L", "\u01cc"], ["\\M-G\\M^M", "\u01cd"], ["\\M-G\\M^N", "\u01ce"], ["\\M-G\\M^O", "\u01cf"], ["\\M-G\\M^P", "\u01d0"], ["\\M-G\\M^Q", "\u01d1"], ["\\M-G\\M^R", "\u01d2"], ["\\M-G\\M^S", "\u01d3"], ["\\M-G\\M^T", "\u01d4"], ["\\M-G\\M^U", "\u01d5"], ["\\M-G\\M^V", "\u01d6"], ["\\M-G\\M^W", "\u01d7"], ["\\M-G\\M^X", "\u01d8"], ["\\M-G\\M^Y", "\u01d9"], ["\\M-G\\M^Z", "\u01da"], ["\\M-G\\M^[", "\u01db"], ["\\M-G\\M^\\", "\u01dc"], ["\\M-G\\M^]", "\u01dd"], ["\\M-G\\M^^", "\u01de"], ["\\M-G\\M^_", "\u01df"], ["\\M-G\\240", "\u01e0"], ["\\M-G\\M-!", "\u01e1"], ["\\M-G\\M-\"", "\u01e2"], ["\\M-G\\M-#", "\u01e3"], ["\\M-G\\M-$", "\u01e4"], ["\\M-G\\M-%", "\u01e5"], ["\\M-G\\M-&", "\u01e6"], ["\\M-G\\M-'", "\u01e7"], ["\\M-G\\M-(", "\u01e8"], ["\\M-G\\M-)", "\u01e9"], ["\\M-G\\M-*", "\u01ea"], ["\\M-G\\M-+", "\u01eb"], ["\\M-G\\M-,", "\u01ec"], ["\\M-G\\M--", "\u01ed"], ["\\M-G\\M-.", "\u01ee"], ["\\M-G\\M-/", "\u01ef"], ["\\M-G\\M-0", "\u01f0"], ["\\M-G\\M-1", "\u01f1"], ["\\M-G\\M-2", "\u01f2"], ["\\M-G\\M-3", "\u01f3"], ["\\M-G\\M-4", "\u01f4"], ["\\M-G\\M-5", "\u01f5"], ["\\M-G\\M-6", "\u01f6"], ["\\M-G\\M-7", "\u01f7"], ["\\M-G\\M-8", "\u01f8"], ["\\M-G\\M-9", "\u01f9"], ["\\M-G\\M-:", "\u01fa"], ["\\M-G\\M-;", "\u01fb"], ["\\M-G\\M-<", "\u01fc"], ["\\M-G\\M-=", "\u01fd"], ["\\M-G\\M->", "\u01fe"], ["\\M-G\\M-?", "\u01ff"], ["\\M-H\\M^@", "\u0200"], ["\\M-H\\M^A", "\u0201"], ["\\M-H\\M^B", "\u0202"], ["\\M-H\\M^C", "\u0203"], ["\\M-H\\M^D", "\u0204"], ["\\M-H\\M^E", "\u0205"], ["\\M-H\\M^F", "\u0206"], ["\\M-H\\M^G", "\u0207"], ["\\M-H\\M^H", "\u0208"], ["\\M-H\\M^I", "\u0209"], ["\\M-H\\M^J", "\u020a"], ["\\M-H\\M^K", "\u020b"], ["\\M-H\\M^L", "\u020c"], ["\\M-H\\M^M", "\u020d"], ["\\M-H\\M^N", "\u020e"], ["\\M-H\\M^O", "\u020f"], ["\\M-H\\M^P", "\u0210"], ["\\M-H\\M^Q", "\u0211"], ["\\M-H\\M^R", "\u0212"], ["\\M-H\\M^S", "\u0213"], ["\\M-H\\M^T", "\u0214"], ["\\M-H\\M^U", "\u0215"], ["\\M-H\\M^V", "\u0216"], ["\\M-H\\M^W", "\u0217"], ["\\M-H\\M^X", "\u0218"], ["\\M-H\\M^Y", "\u0219"], ["\\M-H\\M^Z", "\u021a"], ["\\M-H\\M^[", "\u021b"], ["\\M-H\\M^\\", "\u021c"], ["\\M-H\\M^]", "\u021d"], ["\\M-H\\M^^", "\u021e"], ["\\M-H\\M^_", "\u021f"], ["\\M-H\\240", "\u0220"], ["\\M-H\\M-!", "\u0221"], ["\\M-H\\M-\"", "\u0222"], ["\\M-H\\M-#", "\u0223"], ["\\M-H\\M-$", "\u0224"], ["\\M-H\\M-%", "\u0225"], ["\\M-H\\M-&", "\u0226"], ["\\M-H\\M-'", "\u0227"], ["\\M-H\\M-(", "\u0228"], ["\\M-H\\M-)", "\u0229"], ["\\M-H\\M-*", "\u022a"], ["\\M-H\\M-+", "\u022b"], ["\\M-H\\M-,", "\u022c"], ["\\M-H\\M--", "\u022d"], ["\\M-H\\M-.", "\u022e"], ["\\M-H\\M-/", "\u022f"], ["\\M-H\\M-0", "\u0230"], ["\\M-H\\M-1", "\u0231"], ["\\M-H\\M-2", "\u0232"], ["\\M-H\\M-3", "\u0233"], ["\\M-H\\M-4", "\u0234"], ["\\M-H\\M-5", "\u0235"], ["\\M-H\\M-6", "\u0236"], ["\\M-H\\M-7", "\u0237"], ["\\M-H\\M-8", "\u0238"], ["\\M-H\\M-9", "\u0239"], ["\\M-H\\M-:", "\u023a"], ["\\M-H\\M-;", "\u023b"], ["\\M-H\\M-<", "\u023c"], ["\\M-H\\M-=", "\u023d"], ["\\M-H\\M->", "\u023e"], ["\\M-H\\M-?", "\u023f"], ["\\M-I\\M^@", "\u0240"], ["\\M-I\\M^A", "\u0241"], ["\\M-I\\M^B", "\u0242"], ["\\M-I\\M^C", "\u0243"], ["\\M-I\\M^D", "\u0244"], ["\\M-I\\M^E", "\u0245"], ["\\M-I\\M^F", "\u0246"], ["\\M-I\\M^G", "\u0247"], ["\\M-I\\M^H", "\u0248"], ["\\M-I\\M^I", "\u0249"], ["\\M-I\\M^J", "\u024a"], ["\\M-I\\M^K", "\u024b"], ["\\M-I\\M^L", "\u024c"], ["\\M-I\\M^M", "\u024d"], ["\\M-I\\M^N", "\u024e"], ["\\M-I\\M^O", "\u024f"], ["\\M-M\\M-0", "\u0370"], ["\\M-M\\M-1", "\u0371"], ["\\M-M\\M-2", "\u0372"], ["\\M-M\\M-3", "\u0373"], ["\\M-M\\M-4", "\u0374"], ["\\M-M\\M-5", "\u0375"], ["\\M-M\\M-6", "\u0376"], ["\\M-M\\M-7", "\u0377"], ["\\M-M\\M-8", "\u0378"], ["\\M-M\\M-9", "\u0379"], ["\\M-M\\M-:", "\u037a"], ["\\M-M\\M-;", "\u037b"], ["\\M-M\\M-<", "\u037c"], ["\\M-M\\M-=", "\u037d"], ["\\M-M\\M->", "\u037e"], ["\\M-M\\M-?", "\u037f"], ["\\M-N\\M^@", "\u0380"], ["\\M-N\\M^A", "\u0381"], ["\\M-N\\M^B", "\u0382"], ["\\M-N\\M^C", "\u0383"], ["\\M-N\\M^D", "\u0384"], ["\\M-N\\M^E", "\u0385"], ["\\M-N\\M^F", "\u0386"], ["\\M-N\\M^G", "\u0387"], ["\\M-N\\M^H", "\u0388"], ["\\M-N\\M^I", "\u0389"], ["\\M-N\\M^J", "\u038a"], ["\\M-N\\M^K", "\u038b"], ["\\M-N\\M^L", "\u038c"], ["\\M-N\\M^M", "\u038d"], ["\\M-N\\M^N", "\u038e"], ["\\M-N\\M^O", "\u038f"], ["\\M-N\\M^P", "\u0390"], ["\\M-N\\M^Q", "\u0391"], ["\\M-N\\M^R", "\u0392"], ["\\M-N\\M^S", "\u0393"], ["\\M-N\\M^T", "\u0394"], ["\\M-N\\M^U", "\u0395"], ["\\M-N\\M^V", "\u0396"], ["\\M-N\\M^W", "\u0397"], ["\\M-N\\M^X", "\u0398"], ["\\M-N\\M^Y", "\u0399"], ["\\M-N\\M^Z", "\u039a"], ["\\M-N\\M^[", "\u039b"], ["\\M-N\\M^\\", "\u039c"], ["\\M-N\\M^]", "\u039d"], ["\\M-N\\M^^", "\u039e"], ["\\M-N\\M^_", "\u039f"], ["\\M-N\\240", "\u03a0"], ["\\M-N\\M-!", "\u03a1"], ["\\M-N\\M-\"", "\u03a2"], ["\\M-N\\M-#", "\u03a3"], ["\\M-N\\M-$", "\u03a4"], ["\\M-N\\M-%", "\u03a5"], ["\\M-N\\M-&", "\u03a6"], ["\\M-N\\M-'", "\u03a7"], ["\\M-N\\M-(", "\u03a8"], ["\\M-N\\M-)", "\u03a9"], ["\\M-N\\M-*", "\u03aa"], ["\\M-N\\M-+", "\u03ab"], ["\\M-N\\M-,", "\u03ac"], ["\\M-N\\M--", "\u03ad"], ["\\M-N\\M-.", "\u03ae"], ["\\M-N\\M-/", "\u03af"], ["\\M-N\\M-0", "\u03b0"], ["\\M-N\\M-1", "\u03b1"], ["\\M-N\\M-2", "\u03b2"], ["\\M-N\\M-3", "\u03b3"], ["\\M-N\\M-4", "\u03b4"], ["\\M-N\\M-5", "\u03b5"], ["\\M-N\\M-6", "\u03b6"], ["\\M-N\\M-7", "\u03b7"], ["\\M-N\\M-8", "\u03b8"], ["\\M-N\\M-9", "\u03b9"], ["\\M-N\\M-:", "\u03ba"], ["\\M-N\\M-;", "\u03bb"], ["\\M-N\\M-<", "\u03bc"], ["\\M-N\\M-=", "\u03bd"], ["\\M-N\\M->", "\u03be"], ["\\M-N\\M-?", "\u03bf"], ["\\M-O\\M^@", "\u03c0"], ["\\M-O\\M^A", "\u03c1"], ["\\M-O\\M^B", "\u03c2"], ["\\M-O\\M^C", "\u03c3"], ["\\M-O\\M^D", "\u03c4"], ["\\M-O\\M^E", "\u03c5"], ["\\M-O\\M^F", "\u03c6"], ["\\M-O\\M^G", "\u03c7"], ["\\M-O\\M^H", "\u03c8"], ["\\M-O\\M^I", "\u03c9"], ["\\M-O\\M^J", "\u03ca"], ["\\M-O\\M^K", "\u03cb"], ["\\M-O\\M^L", "\u03cc"], ["\\M-O\\M^M", "\u03cd"], ["\\M-O\\M^N", "\u03ce"], ["\\M-O\\M^O", "\u03cf"], ["\\M-O\\M^P", "\u03d0"], ["\\M-O\\M^Q", "\u03d1"], ["\\M-O\\M^R", "\u03d2"], ["\\M-O\\M^S", "\u03d3"], ["\\M-O\\M^T", "\u03d4"], ["\\M-O\\M^U", "\u03d5"], ["\\M-O\\M^V", "\u03d6"], ["\\M-O\\M^W", "\u03d7"], ["\\M-O\\M^X", "\u03d8"], ["\\M-O\\M^Y", "\u03d9"], ["\\M-O\\M^Z", "\u03da"], ["\\M-O\\M^[", "\u03db"], ["\\M-O\\M^\\", "\u03dc"], ["\\M-O\\M^]", "\u03dd"], ["\\M-O\\M^^", "\u03de"], ["\\M-O\\M^_", "\u03df"], ["\\M-O\\240", "\u03e0"], ["\\M-O\\M-!", "\u03e1