ng $encoding ])

Returns true if the string contains an upper case char, false otherwise.

```php
S::create('fòô bàř', 'UTF-8')->hasUpperCase();
S::hasUpperCase('fòô bàř', 'UTF-8');  // false
```

#### humanize

$stringy->humanize()

S::humanize(string $str [, string $encoding ])

Capitalizes the first word of the string, replaces underscores with
spaces, and strips '_id'.

```php
S::create('author_id')->humanize();
S::humanize('author_id');  // 'Author'
```

#### insert

$stringy->insert(int $index, string $substring)

S::insert(string $str, int $index, string $substring [, string $encoding ])

Inserts $substring into the string at the $index provided.

```php
S::create('fòô bà', 'UTF-8')->insert('ř', 6);
S::insert('fòô bà', 'ř', 6, 'UTF-8');  // 'fòô bàř'
```

#### isAlpha

$stringy->isAlpha()

S::isAlpha(string $str [, string $encoding ])

Returns true if the string contains only alphabetic chars, false otherwise.

```php
S::create('丹尼爾', 'UTF-8')->isAlpha();
S::isAlpha('丹尼爾', 'UTF-8');  // true
```

#### isAlphanumeric

$stringy->isAlphanumeric()

S::isAlphanumeric(string $str [, string $encoding ])

Returns true if the string contains only alphabetic and numeric chars, false
otherwise.

```php
S::create('دانيال1', 'UTF-8')->isAlphanumeric();
S::isAlphanumeric('دانيال1', 'UTF-8');  // true
```

#### isBlank

$stringy->isBlank()

S::isBlank(string $str [, string $encoding ])

Returns true if the string contains only whitespace chars, false otherwise.

```php
S::create("\n\t  \v\f")->isBlank();
S::isBlank("\n\t  \v\f");  // true
```

#### isHexadecimal

$stringy->isHexadecimal()

S::isHexadecimal(string $str [, string $encoding ])

Returns true if the string contains only hexadecimal chars, false otherwise.

```php
S::create('A102F')->isHexadecimal();
S::isHexadecimal('A102F');  // true
```

#### isJson

$stringy->isJson()

S::isJson(string $str [, string $encoding ])

Returns true if the string is JSON, false otherwise.

```php
S::create('{"foo":"bar"}')->isJson();
S::isJson('{"foo":"bar"}');  // true
```

#### isLowerCase

$stringy->isLowerCase()

S::isLowerCase(string $str [, string $encoding ])

Returns true if the string contains only lower case chars, false otherwise.

```php
S::create('fòô bàř', 'UTF-8')->isLowerCase();
S::isLowerCase('fòô bàř', 'UTF-8');  // true
```

#### isSerialized

$stringy->isSerialized()

S::isSerialized(string $str [, string $encoding ])

Returns true if the string is serialized, false otherwise.

```php
S::create('a:1:{s:3:"foo";s:3:"bar";}', 'UTF-8')->isSerialized();
S::isSerialized('a:1:{s:3:"foo";s:3:"bar";}', 'UTF-8');  // true
```

#### isUpperCase

$stringy->isUpperCase()

S::isUpperCase(string $str [, string $encoding ])

Returns true if the string contains only upper case chars, false otherwise.

```php
S::create('FÒÔBÀŘ', 'UTF-8')->isUpperCase();
S::isUpperCase('FÒÔBÀŘ', 'UTF-8');  // true
```

#### last

$stringy->last(int $n)

S::last(int $n [, string $encoding ])

Returns the last $n characters of the string.

```php
S::create('fòô bàř', 'UTF-8')->last(3);
S::last('fòô bàř', 3, 'UTF-8');  // 'bàř'
```

#### length

$stringy->length()

S::length(string $str [, string $encoding ])

Returns the length of the string. An alias for PHP's mb_strlen() function.

```php
S::create('fòô bàř', 'UTF-8')->length();
S::length('fòô bàř', 'UTF-8');  // 7
```

#### longestCommonPrefix

$stringy->longestCommonPrefix(string $otherStr)

S::longestCommonPrefix(string $str, string $otherStr [, $encoding ])

Returns the longest common prefix between the string and $otherStr.

```php
S::create('fòô bar', 'UTF-8')->longestCommonPrefix('fòr bar');
S::longestCommonPrefix('fòô bar', 'fòr bar', 'UTF-8');  // 'fò'
```

#### longestCommonSuffix

$stringy->longestCommonSuffix(string $otherStr)

S::longestCommonSuffix(string $str, string $otherStr [, $encoding ])

Returns the longest common suffix between the string and $otherStr.

```php
S::create('fòô bàř', 'UTF-8')->longestCommonSuffix('fòr bàř');
S::longestCommonSuffix('fòô bàř', 'fòr bàř', 'UTF-8');  // ' bàř'
```

#### longestCommonSubstring

$stringy->longestCommonSubstring(string $otherStr)

S::longestCommonSubstring(string $str, string $otherStr [, $encoding ])

Returns the longest common substring between the string and $otherStr. In the
case of ties, it returns that which occurs first.

```php
S::create('foo bar')->longestCommonSubstring('boo far');
S::longestCommonSubstring('foo bar', 'boo far');  // 'oo '
```

#### lowerCaseFirst

$stringy->lowerCaseFirst();

S::lowerCaseFirst(string $str [, string $encoding ])

Converts the first character of the supplied string to lower case.

```php
S::create('Σ test', 'UTF-8')->lowerCaseFirst();
S::lowerCaseFirst('Σ test', 'UTF-8');  // 'σ test'
```

#### pad

$stringy->pad(int $length [, string $padStr = ' ' [, string $padType = 'right' ]])

S::pad(string $str , int $length [, string $padStr = ' ' [, string $padType = 'right' [, string $encoding ]]])

Pads the string to a given length with $padStr. If length is less than
or equal to the length of the string, no padding takes places. The default
string used for padding is a space, and the default type (one of 'left',
'right', 'both') is 'right'. Throws an InvalidArgumentException if
$padType isn't one of those 3 values.

```php
S::create('fòô bàř', 'UTF-8')->pad( 10, '¬ø', 'left');
S::pad('fòô bàř', 10, '¬ø', 'left', 'UTF-8');  // '¬ø¬fòô bàř'
```

#### padBoth

$stringy->padBoth(int $length [, string $padStr = ' ' ])

S::padBoth(string $str , int $length [, string $padStr = ' ' [, string $encoding ]])

Returns a new string of a given length such that both sides of the string
string are padded. Alias for pad() with a $padType of 'both'.

```php
S::create('foo bar')->padBoth(9, ' ');
S::padBoth('foo bar', 9, ' ');  // ' foo bar '
```

#### padLeft

$stringy->padLeft(int $length [, string $padStr = ' ' ])

S::padLeft(string $str , int $length [, string $padStr = ' ' [, string $encoding ]])

Returns a new string of a given length such that the beginning of the
string is padded. Alias for pad() with a $padType of 'left'.

```php
S::create($str, $encoding)->padLeft($length, $padStr);
S::padLeft('foo bar', 9, ' ');  // '  foo bar'
```

#### padRight

$stringy->padRight(int $length [, string $padStr = ' ' ])

S::padRight(string $str , int $length [, string $padStr = ' ' [, string $encoding ]])

Returns a new string of a given length such that the end of the string is
padded. Alias for pad() with a $padType of 'right'.

```php
S::create('foo bar')->padRight(10, '_*');
S::padRight('foo bar', 10, '_*');  // 'foo bar_*_'
```

#### regexReplace

$stringy->regexReplace(string $pattern, string $replacement [, string $options = 'msr'])

S::regexReplace(string $str, string $pattern, string $replacement [, string $options = 'msr' [, string $encoding ]])

Replaces all occurrences of $pattern in $str by $replacement. An alias
for mb_ereg_replace(). Note that the 'i' option with multibyte patterns
in mb_ereg_replace() requires PHP 5.4+. This is due to a lack of support
in the bundled version of Oniguruma in PHP 5.3.

```php
S::create('fòô ', 'UTF-8')->regexReplace('f[òô]+\s', 'bàř', 'msr');
S::regexReplace('fòô ', 'f[òô]+\s', 'bàř', 'msr', 'UTF-8');  // 'bàř'
```

#### removeLeft

$stringy->removeLeft(string $substring)

S::removeLeft(string $str, string $substring [, string $encoding ])

Returns a new string with the prefix $substring removed, if present.

```php
S::create('fòô bàř', 'UTF-8')->removeLeft('fòô ');
S::removeLeft('fòô bàř', 'fòô ', 'UTF-8');  // 'bàř'
```

#### removeRight

$stringy->removeRight(string $substring)

S::removeRight(string $str, string $substring [, string $encoding ])

Returns a new string with the suffix $substring removed, if present.

```php
S::create('fòô bàř', 'UTF-8')->removeRight(' bàř');
S::removeRight('fòô bàř', ' bàř', 'UTF-8');  // 'fòô'
```

#### replace

$stringy->replace(string $search, string $replacement)

S::replace(string $str, string $search, string $replacement [, string $encoding ])

Replaces all occurrences of $search in $str by $replacement.

```php
S::create('fòô bàř fòô bàř', 'UTF-8')->replace('fòô ', '');
S::replace('fòô bàř fòô bàř', 'fòô ', '', 'UTF-8');  // 'bàř bàř'
```

#### reverse

$stringy->reverse()

S::reverse(string $str [, string $encoding ])

Returns a reversed string. A multibyte version of strrev().

```php
S::create('fòô bàř', 'UTF-8')->reverse();
S::reverse('fòô bàř', 'UTF-8');  // 'řàb ôòf'
```

#### safeTruncate

$stringy->safeTruncate(int $length [, string $substring = '' ])

S::safeTruncate(string $str, int $length [, string $substring = '' [, string $encoding ]])

Truncates the string to a given length, while ensuring that it does not
split words. If $substring is provided, and truncating occurs, the
string is further truncated so that the substring may be appended without
exceeding the desired length.

```php
S::create('What are your plans today?')->safeTruncate(22, '...');
S::safeTruncate('What are your plans today?', 22, '...');  // 'What are your plans...'
```

#### shuffle

$stringy->shuffle()

S::shuffle(string $str [, string $encoding ])

A multibyte str_shuffle() function. It returns a string with its characters in
random order.

```php
S::create('fòô bàř', 'UTF-8')->shuffle();
S::shuffle('fòô bàř', 'UTF-8');  // 'àôřb òf'
```

#### slugify

$stringy->slugify([ string $replacement = '-' ])

S::slugify(string $str [, string $replacement = '-' ])

Converts the string into an URL slug. This includes replacing non-ASCII
characters with their closest ASCII equivalents, removing remaining
non-ASCII and non-alphanumeric characters, and replacing whitespace with
$replacement. The replacement defaults to a single dash, and the string
is also converted to lowercase.

```php
S::create('Using strings like fòô bàř')->slugify();
S::slugify('Using strings like fòô bàř');  // 'using-strings-like-foo-bar'
```

#### startsWith

$stringy->startsWith(string $substring [, boolean $caseSensitive = true ])

S::startsWith(string $str, string $substring [, boolean $caseSensitive = true [, string $encoding ]])

Returns true if the string begins with $substring, false otherwise.
By default, the comparison is case-sensitive, but can be made insensitive
by setting $caseSensitive to false.

```php
S::create('FÒÔ bàřs', 'UTF-8')->startsWith('fòô bàř', false);
S::startsWith('FÒÔ bàřs', 'fòô bàř', false, 'UTF-8');  // true
```

#### substr

$stringy->substr(int $start [, int $length ])

S::substr(string $str, int $start [, int $length [, string $encoding ]])

Returns the substring beginning at $start with the specified $length.
It differs from the mb_substr() function in that providing a $length of
null will return the rest of the string, rather than an empty string.

```php
S::create('fòô bàř', 'UTF-8')->substr(2, 3);
S::substr('fòô bàř', 2, 3, 'UTF-8');  // 'ô b'
```

#### surround

$stringy->surround(string $substring)

S::surround(string $str, string $substring)

Surrounds a string with the given substring.

```php
S::create(' ͜ ')->surround('ʘ');
S::surround(' ͜ ', 'ʘ');  // 'ʘ ͜ ʘ'
```

#### swapCase

$stringy->swapCase();

S::swapCase(string $str [, string $encoding ])

Returns a case swapped version of the string.

```php
S::create('Ντανιλ', 'UTF-8')->swapCase();
S::swapCase('Ντανιλ', 'UTF-8');  // 'νΤΑΝΙΛ'
```

#### tidy

$stringy->tidy()

S::tidy(string $str)

Returns a string with smart quotes, ellipsis characters, and dashes from
Windows-1252 (commonly used in Word documents) replaced by their ASCII equivalents.

```php
S::create('“I see…”')->tidy();
S::tidy('“I see…”');  // '"I see..."'
```

#### titleize

$stringy->titleize([ string $encoding ])

S::titleize(string $str [, array $ignore [, string $encoding ]])

Returns a trimmed string with the first letter of each word capitalized.
Ignores the case of other letters, preserving any acronyms. Also accepts
an array, $ignore, allowing you to list words not to be capitalized.

```php
$ignore = array('at', 'by', 'for', 'in', 'of', 'on', 'out', 'to', 'the');
S::create('i like to watch DVDs at home', 'UTF-8')->titleize($ignore);
S::titleize('i like to watch DVDs at home', $ignore, 'UTF-8');
// 'I Like to Watch DVDs at Home'
```

#### toAscii

$stringy->toAscii()

S::toAscii(string $str [, boolean $removeUnsupported = true])

Returns an ASCII version of the string. A set of non-ASCII characters are
replaced with their closest ASCII counterparts, and the rest are removed
unless instructed otherwise.

```php
S::create('fòô bàř')->toAscii();
S::toAscii('fòô bàř');  // 'foo bar'
```

#### toLowerCase

$stringy->toLowerCase()

S::toLowerCase(string $str [, string $encoding ])

Converts all characters in the string to lowercase. An alias for PHP's
mb_strtolower().

```php
S::create('FÒÔ BÀŘ', 'UTF-8')->toLowerCase();
S::toLowerCase('FÒÔ BÀŘ', 'UTF-8');  // 'fòô bàř'
```

#### toSpaces

$stringy->toSpaces([ tabLength = 4 ])

S::toSpaces(string $str [, int $tabLength = 4 ])

Converts each tab in the string to some number of spaces, as defined by
$tabLength. By default, each tab is converted to 4 consecutive spaces.

```php
S::create(' String speech = "Hi"')->toSpaces();
S::toSpaces('   String speech = "Hi"');  // '    String speech = "Hi"'
```

#### toTabs

$stringy->toTabs([ tabLength = 4 ])

S::toTabs(string $str [, int $tabLength = 4 ])

Converts each occurrence of some consecutive number of spaces, as defined
by $tabLength, to a tab. By default, each 4 consecutive spaces are
converted to a tab.

```php
S::create('    fòô    bàř')->toTabs();
S::toTabs('    fòô    bàř');  // '   fòô bàř'
```

#### toTitleCase

$stringy->toTitleCase()

S::toTitleCase(string $str [, string $encoding ])

Converts the first character of each word in the string to uppercase.

```php
S::create('fòô bàř', 'UTF-8')->toTitleCase();
S::toTitleCase('fòô bàř', 'UTF-8');  // 'Fòô Bàř'
```

#### toUpperCase

$stringy->toUpperCase()

S::toUpperCase(string $str [, string $encoding ])

Converts all characters in the string to uppercase. An alias for PHP's
mb_strtoupper().

```php
S::create('fòô bàř', 'UTF-8')->toUpperCase();
S::toUpperCase('fòô bàř', 'UTF-8');  // 'FÒÔ BÀŘ'
```

#### trim

$stringy->trim()

S::trim(string $str)

Returns the trimmed string. An alias for PHP's trim() function.

```php
S::create('fòô bàř', 'UTF-8')->trim();
S::trim(' fòô bàř ');  // 'fòô bàř'
```

#### truncate

$stringy->truncate(int $length [, string $substring = '' ])

S::truncate(string $str, int $length [, string $substring = '' [, string $encoding ]])

Truncates the string to a given length. If $substring is provided, and
truncating occurs, the string is further truncated so that the substring
may be appended without exceeding the desired length.

```php
S::create('What are your plans today?')->truncate(19, '...');
S::truncate('What are your plans today?', 19, '...');  // 'What are your pl...'
```

#### underscored

$stringy->underscored();

S::underscored(string $str [, string $encoding ])

Returns a lowercase and trimmed string separated by underscores.
Underscores are inserted before uppercase characters (with the exception
of the first character of the string), and in place of spaces as well as dashes.

```php
S::create('TestUCase')->underscored();
S::underscored('TestUCase');  // 'test_u_case'
```

#### upperCamelize

$stringy->upperCamelize();

S::upperCamelize(string $str [, string $encoding ])

Returns an UpperCamelCase version of the supplied string. It trims
surrounding spaces, capitalizes letters following digits, spaces, dashes
and underscores, and removes spaces, dashes, underscores.

```php
S::create('Upper Camel-Case')->upperCamelize();
S::upperCamelize('Upper Camel-Case');  // 'UpperCamelCase'
```

#### upperCaseFirst

$stringy->upperCaseFirst();

S::upperCaseFirst(string $str [, string $encoding ])

Converts the first character of the supplied string to upper case.

```php
S::create('σ test', 'UTF-8')->upperCaseFirst();
S::upperCaseFirst('σ test', 'UTF-8');  // 'Σ test'
```

## Links

The following is a list of libraries that extend Stringy:

 * [SliceableStringy](https://github.com/danielstjules/SliceableStringy):
Python-like string slices in PHP

## Tests

From the project directory, tests can be ran using `phpunit`

## License

Released under the MIT License - see `LICENSE.txt` for details.
                                                                                                                                                                                                                         �8��	�YH�4�����"�d+m*9��j�B3��L��9�N9ɨ����g҆!�3����tm���琁��M �cy�4 ,�n+�$�b�ѰG\��M�~a��&��^&���1�� R��;f���c�FI�H��F�x����.�XJ�'<{T-w*����D��Yd?J_�����搂m^�)6,�� }��27�?S@"� 4���*i/� �L�֘&,�.��T�]�t�;�:��vܟJ"3����kN�|˜�7�?��^Y��h�w�01@�!�Tr�?,܊A ���:�K?Z $V`�Q��
	�ȕ[y���$�h�۞9�֗r�P fa�^>����q#ހ�����p��cހ�s�sMs�@� #'r�}*U.ѝßZ �:���Q�F�  \�H�J�1�ܓ@���"�$� 7�lm�i��p����X.��h*U�O�@ls����$JrŹ�d�o!��l*Ch�a#h���}�<�,jO82�
 bL��cMvv9��h����F� 5���h1��g� 6�n1L<t4 �3q�i��	�(���{әԿ�
c sLUv|���+*��,��P�]��Cu�p3LD���&����@�6��}�%f\��h t��)�w�@󔓿��YQI+�h�N���5��l��Ruf9��x �a��֘y�q@
H����(c88���� ������b�#n�R�$�e��iA�� )!�&�H<� /�Z\pA� Nq�ތd��( c�;p� 3������d��J�Xg� #q�i'� 8��"��Ty��>����@9���h�)��84 4�49�^hʅ����� "��&���i�7sg�hX'=~�f"I���,��)z����#"L�A��и��~he������;��"U'�[�J���hY��Rı����0-BѤ{G����8���h��Lr�0 E$�ПsHg\ M W��ˎM5Q�s���E��6@�h$b�7�L�"�Bİ��eM�!iteu��5&,0GJ�;�Ȼg#ןZ���s)ǭz�_�y���	�ϕs�T��1�Ew���0�\�\���"�n�ڼ���@���CF�c�5����l���ɠ�.|���j��d�g<��H��a'~��ήq楉1�G��LB���P�w�Lg�@D��( ��í 2����Z�ve �� ߝ�e=i͍�qϭ +L�[��Ѐ�z�4 �le<�*9Y�6� ��s�L�I�b��M&ry��͊S�9�-������Z Vnqր�T����� U*�����p�4�iD�Ҁ�O"��(ˏ^i��� ��N�RC� F#R�p4 =)Ԋ o$�J\hI9�ԁ�=h��Lwbr�h	旿�@
ۈ�L �@qG�s��@Tܹ-M
I�h�8{R� ��5	4 �3��O�W$�8U�<�a��@��s�����j c��`3g� # [)�~�E &����I�4&��6�֚
���h�9�׭ .���Fô����cF�
hrH�֌�����A`xh�=.>s������z\�L� JYUT�0�Ėj c9sӊ�Z }hvǌ�� Za����M07V�^89��C@w�Ro8�(X ����Gʯ֐�r})CJ;qހ�w�F �QBXe�g&�$
���pi�W'� ��ZHف�$��Ƚ�1���0=h�b����P��y��W`ÌR�,� 0�"����D��B�ך}�����	p�'�'֪K�vc ��Ld�
� �:TpQ��d�� .<�py5"���c��@�n�zS�£y�"hɑw��C�m��[�����Y�wYx�;� �K��n��e�P"�c�����"��n�<�-�3r:��^B@-Ai����I�9��Iv�h)1�包>�,s��c��e��?^���ޭ�<�*Z��kYk��d�q�]�.�OZ�ډ�cU�	�$\���E�q�1$�z��	<�iF/Ȧ x��	*F@C�j+�f���@�1��p�MF����(Y!n�E�@7�`.�,y&�ݟ�LLz��RD�ÏZB�RT�i�A� 
3�R1Q�(���3'��P�|��K��Ni��)<�C�$@���&��4��<�N0O3���p��^�=jAj\�Ȯ�b�{�6�eOz�Mq��ڲ{����\()��ԷV��yCґDsiq>
B���3D����12TҬ,`�`�$���a�������md�*tH�௵ZB!�Ð̥�1�T	��a� Fz�XE��=nSy]�t���[_-�6	}�4	�'����du$R��rp�VOҠ�OB	-҉�3
��X��1��1��-w�UY<"ی�x����7�c%s?�Ia�u��_,�Ӱ���+䏻�Q���V	�?v��������:��dl�`��Ԛb����=6_*
�`M��~UP���"��°��ұ7#������ ����y��J�Ɲ�LK�
J:�	<�J��=,��@1JŢ�jR��0i���ٺF��t��ț�W!��F$M,��X�ˉ���"�ٝCM�O�S�ò��1�Ԭ"��,DF�=�PZhWe��ө�a�$���!�<�)�E�!����ލ40����V�K��F�0%:U�_�#�Z��3n3�@24�sg��lzc�SHA&�z�~����k1NP�:�+l�&��jU�t�-0#���*(�_�+(l�@�����yɤhA�8� [��T+X��d>>��#�ԙ 㷵7�gc�����zq��M*�]�U����K�FQ��29`�۷}*��H�1�qLi� �z�ʇ#�!p<c�QƱ�%�GA� s��+g�`
�^�� B��'�C�nz1T����I�~^�h�FK���ԛ��4 �+�3C2�y��X��0ܒ8�S@W^�jFY��Nh6C��=�7� ^�%��� ��@Ė�HE;NhA![4�$��ҀY�8��~t �:�_�v^wPon�Ҟ$J� 7�,��� 1ߠ�"�m�u�ulu��̼c�J ��)�#� yWeݎ��P#c< GpįOzf�a��hQAo��SN;O �J�8�M
Crh��6qJŔ��������x�$ � 7��w����3@�h�C#��W.��4�+)�����|���@� ���Q�ś�֐ĒM�q���0y �2�L�� ����d�~�h0��~��x�y�T�hKP�?-D���NE< $�&�x�� ���VM 7�w�Bw/"�0	���� ٥a��� 4rriU�9����_ʂ��� 5�NOJBH ޴m�z p Ӊ,�1@	���h�-���r�Av=h %�9��͒(0捤s@�G�@�y���J=h̃�}� gހ$��J��~�0c�5HdP�נ� eM٥��!�Q�u�Ң��D:Ԩ9��	p���ZP!��4�s79PjP��9sL:�OJ��r�� � bܹ'�J�Բ�oC@�rg ҡ�`7=��;<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'CreateBrandsTable' => $baseDir . '/database/migrations/2015_03_08_125400_create_brands_table.php',
    'CreateColorsTable' => $baseDir . '/database/migrations/2015_03_08_125408_create_colors_table.php',
    'CreateCommentsTable' => $baseDir . '/database/migrations/2015_03_08_125638_create_comments_table.php',
    'CreateConditionsTable' => $baseDir . '/database/migrations/2015_03_08_125417_create_conditions_table.php',
    'CreateLocationsTable' => $baseDir . '/database/migrations/2015_03_08_125315_create_locations_table.php',
    'CreatePostsTable' => $baseDir . '/database/migrations/2015_03_08_125551_create_posts_table.php',
    'CreateShoeTypesTable' => $baseDir . '/database/migrations/2015_03_08_125511_create_shoe_types_table.php',
    'CreateSizesTable' => $baseDir . '/database/migrations/2015_03_08_125427_create_sizes_table.php',
    'CreateStatusesTable' => $baseDir . '/database/migrations/2015_03_08_125453_create_statuses_table.php',
    'CreateUserStatusesTable' => $baseDir . '/database/migrations/2015_03_08_125341_create_user_statuses_table.php',
    'CreateUserTypesTable' => $baseDir . '/database/migrations/2015_03_08_125332_create_user_types_table.php',
    'CreateUsersTable' => $baseDir . '/database/migrations/2015_03_08_125525_create_users_table.php',
    'DatabaseSeeder' => $baseDir . '/database/seeds/DatabaseSeeder.php',
    'File_Iterator' => $vendorDir . '/phpunit/php-file-iterator/File/Iterator.php',
    'File_Iterator_Facade' => $vendorDir . '/phpunit/php-file-iterator/File/Iterator/Facade.php',
    'File_Iterator_Factory' => $vendorDir . '/phpunit/php-file-iterator/File/Iterator/Factory.php',
    'IlluminateQueueClosure' => $vendorDir . '/laravel/framework/src/Illuminate/Queue/IlluminateQueueClosure.php',
    'PHPUnit_Exception' => $vendorDir . '/phpunit/phpunit/src/Exception.php',
    'PHPUnit_Extensions_GroupTestSuite' => $vendorDir . '/phpunit/phpunit/src/Extensions/GroupTestSuite.php',
    'PHPUnit_Extensions_PhptTestCase' => $vendorDir . '/phpunit/phpunit/src/Extensions/PhptTestCase.php',
    'PHPUnit_Extensions_PhptTestSuite' => $vendorDir . '/phpunit/phpunit/src/Extensions/PhptTestSuite.php',
    'PHPUnit_Extensions_RepeatedTest' => $vendorDir . '/phpunit/phpunit/src/Extensions/RepeatedTest.php',
    'PHPUnit_Extensions_TestDecorator' => $vendorDir . '/phpunit/phpunit/src/Extensions/TestDecorator.php',
    'PHPUnit_Extensions_TicketListener' => $vendorDir . '/phpunit/phpunit/src/Extensions/TicketListener.php',
    'PHPUnit_Framework_Assert' => $vendorDir . '/phpunit/phpunit/src/Framework/Assert.php',
    'PHPUnit_Framework_AssertionFailedError' => $vendorDir . '/phpunit/phpunit/src/Framework/AssertionFailedError.php',
    'PHPUnit_Framework_BaseTestListener' => $vendorDir . '/phpunit/phpunit/src/Framework/BaseTestListener.php',
    'PHPUnit_Framework_CodeCoverageException' => $vendorDir . '/phpunit/phpunit/src/Framework/CodeCoverageException.php',
    'PHPUnit_Framework_Constraint' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint.php',
    'PHPUnit_Framework_Constraint_And' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/And.php',
    'PHPUnit_Framework_Constraint_ArrayHasKey' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/ArrayHasKey.php',
    'PHPUnit_Framework_Constraint_ArraySubset' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/ArraySubset.php',
    'PHPUnit_Framework_Constraint_Attribute' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/Attribute.php',
    'PHPUnit_Framework_Constraint_Callback' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/Callback.php',
    'PHPUnit_Framework_Constraint_ClassHasAttribute' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/ClassHasAttribute.php',
    'PHPUnit_Framework_Constraint_ClassHasStaticAttribute' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/ClassHasStaticAttribute.php',
    'PHPUnit_Framework_Constraint_Composite' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/Composite.php',
    'PHPUnit_Framework_Constraint_Count' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/Count.php',
    'PHPUnit_Framework_Constraint_Exception' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/Exception.php',
    'PHPUnit_Framework_Constraint_ExceptionCode' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/ExceptionCode.php',
    'PHPUnit_Framework_Constraint_ExceptionMessage' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/ExceptionMessage.php',
    'PHPUnit_Framework_Constraint_ExceptionMessageRegExp' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/ExceptionMessageRegExp.php',
    'PHPUnit_Framework_Constraint_FileExists' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/FileExists.php',
    'PHPUnit_Framework_Constraint_GreaterThan' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/GreaterThan.php',
    'PHPUnit_Framework_Constraint_IsAnything' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/IsAnything.php',
    'PHPUnit_Framework_Constraint_IsEmpty' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/IsEmpty.php',
    'PHPUnit_Framework_Constraint_IsEqual' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/IsEqual.php',
    'PHPUnit_Framework_Constraint_IsFalse' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/IsFalse.php',
    'PHPUnit_Framework_Constraint_IsIdentical' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/IsIdentical.php',
    'PHPUnit_Framework_Constraint_IsInstanceOf' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/IsInstanceOf.php',
    'PHPUnit_Framework_Constraint_IsJson' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/IsJson.php',
    'PHPUnit_Framework_Constraint_IsNull' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/IsNull.php',
    'PHPUnit_Framework_Constraint_IsTrue' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/IsTrue.php',
    'PHPUnit_Framework_Constraint_IsType' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/IsType.php',
    'PHPUnit_Framework_Constraint_JsonMatches' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/JsonMatches.php',
    'PHPUnit_Framework_Constraint_JsonMatches_ErrorMessageProvider' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/JsonMatches/ErrorMessageProvider.php',
    'PHPUnit_Framework_Constraint_LessThan' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/LessThan.php',
    'PHPUnit_Framework_Constraint_Not' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/Not.php',
    'PHPUnit_Framework_Constraint_ObjectHasAttribute' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/ObjectHasAttribute.php',
    'PHPUnit_Framework_Constraint_Or' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/Or.php',
    'PHPUnit_Framework_Constraint_PCREMatch' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/PCREMatch.php',
    'PHPUnit_Framework_Constraint_SameSize' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/SameSize.php',
    'PHPUnit_Framework_Constraint_StringContains' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/StringContains.php',
    'PHPUnit_Framework_Constraint_StringEndsWith' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/StringEndsWith.php',
    'PHPUnit_Framework_Constraint_StringMatches' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/StringMatches.php',
    'PHPUnit_Framework_Constraint_StringStartsWith' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/StringStartsWith.php',
    'PHPUnit_Framework_Constraint_TraversableContains' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/TraversableContains.php',
    'PHPUnit_Framework_Constraint_TraversableContainsOnly' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/TraversableContainsOnly.php',
    'PHPUnit_Framework_Constraint_Xor' => $vendorDir . '/phpunit/phpunit/src/Framework/Constraint/Xor.php',
    'PHPUnit_Framework_Error' => $vendorDir . '/phpunit/phpunit/src/Framework/Error.php',
    'PHPUnit_Framework_Error_Deprecated' => $vendorDir . '/phpunit/phpunit/src/Framework/Error/Deprecated.php',
    'PHPUnit_Framework_Error_Notice' => $vendorDir . '/phpunit/phpunit/src/Framework/Error/Notice.php',
    'PHPUnit_Framework_Error_Warning' => $vendorDir . '/phpunit/phpunit/src/Framework/Error/Warning.php',
    'PHPUnit_Framework_Exception' => $vendorDir . '/phpun